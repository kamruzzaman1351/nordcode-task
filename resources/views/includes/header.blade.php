<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'NordCode') }}
            </a>
        </div>
    </div>
</nav>
