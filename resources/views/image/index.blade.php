@extends('layouts.master')

@section('content')
    <div class="row mb-4">
        {{-- Message --}}
        <div class="col-md-6 offset-md-2">
            @include('includes.messages')
        </div>
        {{-- Form --}}
        <div class="col-sm-12 p-4">
            <div class="card">
                <div class="card-body" style="padding-bottom: 0;">
                    {!!  Form::open(array('action' => 'ImageUploadController@storeImage', 'method' => 'POST','enctype' => 'multipart/form-data')) !!}
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-5">
                            {!! Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Image Title' ))!!}
                        </div>
                        <div class="form-group col-md-5">
                            <label class="custom-file" style="width: 100%;">
                                <input type="file" name="img_name" id="file" class="custom-file-input">
                                <span class="custom-file-control"></span>
                            </label>
                        </div>
                        <div class="form-group col-md-2">
                        {!! Form::submit('Upload Image', array('class'=>'btn btn-primary btn-block'))!!}
                        </div>
                    </div>


                    {!!  Form::close() !!}


                </div>
            </div>
        </div>
    </div>

    {{-- Image Show --}}
    <div class="row">
        @if (count($images)>0)

            @foreach ($images as $image)

                <div class="col-md-4 mb-3">
                    <div class="card" >
                        <img src="{{'upImages/'.$image->img_name}}" class="card-img-top" alt="{{$image->title}}" width="330px" height="330px">
                        <div class="card-body">
                            <h5 class="card-title">{{$image->title}}</h5>
                            <a href="{{'/'.$image->id}}" class="btn btn-primary">View Image</a>
                        </div>
                    </div>
                </div>

            @endforeach

            @else
                <h2>No Image Uploaded yet.</h2>
        @endif

    </div>

    {{-- Pagination --}}
    <div class="row mb-3">
        {{ $images->links() }}

    </div>

@endsection


