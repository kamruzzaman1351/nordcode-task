@extends('layouts.master')

@section('content')
    <div class="col-md-8 offset-md-2">
        <div class="card" >
            <img src="{{'upImages/'.$image->img_name}}" class="card-img-top" alt="{{$image->title}}">

            <div class="card-body">
                <h5 class="card-title">{{$image->title}}</h5>
                <a href="/" class="btn btn-primary">Go Back</a>
            </div>
        </div>
    </div>

@endsection
