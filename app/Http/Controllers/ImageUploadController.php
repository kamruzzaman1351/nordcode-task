<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;

class ImageUploadController extends Controller
{
    // Show All Images

    public function index()
    {
        $images = Image::orderBy('id', 'DESC')->paginate(9);

        return view('image.index', compact('images'));
    }

    // Show Single Image

    public function singleImage($id)
    {
        $image = Image::findOrFail($id);

        return view('image.show', compact('image'));
    }

    // Store Image

    public function storeImage(Request $request)
    {
        $this->validate($request, [
            'title'     => 'required|max:100',
            'img_name'  => 'required|image|mimes:jpg,png,gif|max:2048'
        ]);


        // Handle Image Upload

        if ($request->hasFile('img_name')) {

            // Get Image Name with the extension
            $filenameWithExt = $request->file('img_name')->getClientOriginalName();

            // Get just Image Name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            // Get just ext
            $extention = $request->file('img_name')->getClientOriginalExtension();

            // Image Name to Store
            $fileNameToStore = $filename . '_' . time() . '.' . $extention;

            // Upload Image path
            $path = $request->file('img_name')->move('upImages', $fileNameToStore);
        }

        // Strong Image to DB

        $image = new Image;
        $image->title       = $request->input('title');
        $image->img_name    = $fileNameToStore;
        $image->save();

        return redirect('/')->with('success', 'Image Uploaded Successfully');
    }
}
